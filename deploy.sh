#!/bin/env bash
yarn build && rsync -r --no-t --checksum dist/ upload && aws s3 sync ./upload s3://shapeybattles/ --acl public-read
