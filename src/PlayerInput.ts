import Vector2 = Phaser.Math.Vector2;
import InputPlugin = Phaser.Input.InputPlugin;
import Camera = Phaser.Cameras.Scene2D.Camera;
import Ship from "./ship";
import {HUDScene} from "./hud";
import {GameScene} from "./GameScene";


export class PlayerInput extends Phaser.Events.EventEmitter {
    movementVector: Vector2 = new Vector2(0, 0);
    firingVector: Vector2 = new Vector2(0, 0);
    useKBM: boolean;
    useTouch: boolean;
    gamepadNumber: number;
    inputPlugin: InputPlugin;
    gameScene: GameScene;
    keys = {
        up: null,
        down: null,
        left: null,
        right: null,
        bomb: null
    };
    bombLocked: string = null;

    constructor (scene: GameScene, useKBM: boolean = true, useTouch: boolean = false, padNumber: number = -1) {
        super();
        console.log("initializing player input with pad#", padNumber)
        this.inputPlugin = scene.input;
        this.gameScene = scene;
        this.useTouch = useTouch;
        if (this.useTouch) {
            this.inputPlugin.addPointer(2);
        }
        this.gamepadNumber = padNumber;
        this.useKBM = useKBM;
        if (useKBM) {
            this.keys.up = this.inputPlugin.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W)
            this.keys.down = this.inputPlugin.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S)
            this.keys.left = this.inputPlugin.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A)
            this.keys.right = this.inputPlugin.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D)
            this.keys.bomb = this.inputPlugin.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        }
    }

    setMovementVector(x, y) {
        const wasMoving = this.movementVector.length() > 0.15;
        this.movementVector.set(x, y)
        const isMoving = this.movementVector.length() > 0.15;
        if (wasMoving && !isMoving) {
            this.emit('stopmoving');
        } else if (isMoving && !wasMoving) {
            this.emit('startmoving');
        }
    }

    update (ship: Ship, camera: Camera) {
        const hud = this.gameScene.scene.get('hud') as HUDScene;
        const pad = this.inputPlugin.gamepad.gamepads[this.gamepadNumber];
        if (pad && pad.leftStick.length() > 0.15) {
            this.setMovementVector(pad.leftStick.x, pad.leftStick.y);
        } else if (this.useTouch && hud.leftStick.vector.length() > 0.15) {
            this.setMovementVector(hud.leftStick.vector.x, hud.leftStick.vector.y);
        } else if (this.useKBM) {
            let vx = 0;
            let vy = 0;
            if (this.keys.left.isDown) vx -= 1;
            if (this.keys.right.isDown) vx += 1;
            if (this.keys.up.isDown) vy -= 1;
            if (this.keys.down.isDown) vy += 1;
            if (vx !== 0 && vy !== 0) {
                vx *= 0.77;
                vy *= 0.77;
            }
            this.setMovementVector(vx, vy);
        } else {
            this.setMovementVector(0, 0);
        }

        if (pad && pad.rightStick.length() > 0.15) {
            this.firingVector.set(pad.rightStick.x, pad.rightStick.y);
            this.firingVector = this.firingVector.normalize()
        } else if (this.useTouch && hud.rightStick.vector.length() > 0.15) {
            this.firingVector.set(hud.rightStick.vector.x, hud.rightStick.vector.y);
            this.firingVector = this.firingVector.normalize();
        } else if (this.useKBM) {
           if (this.inputPlugin.mousePointer.leftButtonDown()) {
               const adjPtr: Vector2 = this.inputPlugin.mousePointer.positionToCamera(camera) as Vector2;
               const pVec = new Vector2(adjPtr.x - ship.x, adjPtr.y - ship.y).normalize();
               this.firingVector.set(pVec.x, pVec.y);
           } else {
               this.firingVector.set(0, 0);
           }
        }

        if (pad && pad.R2 && !this.bombLocked) {
            console.log('bombing', pad.R2, this.bombLocked)
            this.bombLocked = 'pad';
            this.emit('bomb');
        } else if (pad && !pad.R2 && this.bombLocked === 'pad') {
            this.bombLocked = null;
            console.log('stop bombing')
        }

        if (this.useKBM) {
            if (this.keys.bomb.isDown && !this.bombLocked) {
                this.bombLocked = 'kbm';
                this.emit('bomb')
            } else if (!this.keys.bomb.isDown && this.bombLocked === 'kbm') {
                this.bombLocked = null;
            }
        }

        if (this.useTouch) {
            if (hud.bombButton && !this.bombLocked) {
                this.bombLocked = 'touch'
                this.emit('bomb');
            } else if (!hud.bombButton && this.bombLocked === 'touch') {
                this.bombLocked = null;
            }
        }
    }
}
