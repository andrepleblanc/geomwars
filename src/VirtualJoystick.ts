import * as Phaser from 'phaser';

export class VirtualJoystick extends Phaser.GameObjects.Graphics {

    vector: Phaser.Math.Vector2;
    startVec: Phaser.Math.Vector2;

    down: boolean = false;
    private width: number;
    private height: number;
    constructor(scene: Phaser.Scene, x: number, y: number, width: number, height: number) {
        super(scene, {
            x: x,
            y: y,
            lineStyle: {
                width: 1,
                color: 0xFFFFFF,
            },
            fillStyle: {
                color: 0xFFFFFF,
                alpha: 1
            }
        });
        this.startVec = new Phaser.Math.Vector2(0, 0);
        this.width = width;
        this.alpha = 0.01;
        this.height = height;
        this.vector = new Phaser.Math.Vector2(0, 0);
        const rect = new Phaser.Geom.Rectangle(0, 0, this.width, this.height);
        this.setInteractive(rect, (hitArea: Phaser.Geom.Rectangle, x, y) => {
            return hitArea.contains(x, y);
        })
        this.on('pointerdown', this.onPointerDown, this);
        this.on('pointermove', this.onPointerMove, this);
        this.on('pointerup', this.onPointerUp, this);
        this.on('pointerout', this.onPointerUp, this);
    }

    onPointerDown (event) {
        this.alpha = 0.25
        this.startVec.set(event.x - this.x, event.y - this.y);
        this.down = true;
        console.log('down')
    }

    onPointerUp(event) {
        this.alpha = 0.01;
        this.down = false;
        console.log('up')
        this.vector.set(0, 0);
    }

    onPointerMove(event) {
        if (this.down) {
            console.log('move')
            this.vector.set((event.x - this.x - this.startVec.x) / 128, (event.y - this.y - this.startVec.y) / 128);
            if (this.vector.length() > 1) {
                this.vector = this.vector.normalize();
            }
        }
    }

    preUpdate() {
        this.clear();
        this.strokeCircle(this.startVec.x, this.startVec.y, 128);
        this.fillCircle(this.startVec.x + this.vector.x * 128, this.startVec.y + this.vector.y * 128, 64);
    }


}