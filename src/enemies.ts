import Ship from "./ship";
import Vector2 = Phaser.Math.Vector2;
import ParticleEmitter = Phaser.GameObjects.Particles.ParticleEmitter;


export class Enemy extends Phaser.Physics.Arcade.Image {
    static EXPLODER_PARTICLES = null;
    static EMITTER_COLOR_MAP = new Map();
    score: number = 100;
    private spawntime: number = 1000;
    spawned: boolean = false;
    deflecting: boolean = false;
    phased: boolean = false;
    wave: any;
    exploder: ParticleEmitter;
    color: number = 0xFFFFFF;

    constructor(scene: Phaser.Scene, x: number, y: number, texture?: string | Phaser.Textures.Texture, color?: number) {
        super(scene, x, y, texture);
        this.color = color;
        if (Enemy.EXPLODER_PARTICLES === null) {
            Enemy.initExploder(scene);
        }
        scene.physics.add.existing(this, false);
        scene.add.existing(this);
        this.setTint(color);
        this.exploder = Enemy.getExploder(this.color);
        this.setCollideWorldBounds(true, 0.5, 0.5);
    }

    static getExploder(color: number) {
        if (!this.EMITTER_COLOR_MAP.has(color)) {
            this.EMITTER_COLOR_MAP.set(color, this.EXPLODER_PARTICLES.createEmitter({
                angle: { min: 0, max: 360 },
                lifespan: 666,
                tint: color,
                speed: { min: 100, max: 400 },
                quantity: { min: 4, max: 8 },
                scale: { start: 2, end: 0 },
                rotate: { start: 0, end: 360, random: true },
                on: false
            }))
        }
        return this.EMITTER_COLOR_MAP.get(color);

    }

    static initExploder(scene: Phaser.Scene) {
        this.EXPLODER_PARTICLES = scene.add.particles('ship_pieces');
    }

    update (t, delta) {
        if (!this.spawned) {
            this.spawntime -= delta;
            this.setAlpha(Phaser.Math.Percent(1000-this.spawntime, 0, 1000));
            if (this.spawntime <= 0) {
                this.spawned = true;
                this.setAlpha(1);
            }
        }
    }

    explode() {
        if (this.exploder) {
            this.exploder.emitParticleAt(this.x, this.y);
        }
    }
}

export class Circle extends Enemy {

    static EXPLODER: Phaser.GameObjects.Particles.ParticleEmitterManager;
    static CHARGE_TIME = 2000;
    static MAX_RUN_TIME = 5000;
    chargeTime: number = 0;
    runTime: number = Circle.MAX_RUN_TIME;
    charging: boolean = true;
    target: Vector2;

    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'circle', 0xFF0000);
        this.target = new Vector2(0, 0);
        this.setCircle(48);
        this.setOffset(16, 16)
        this.setMass(20);
        this.setFriction(0, 0);
        this.setGravity(0, 0);
        this.setDrag(0,0);
        this.setGravity(0, 0);
        this.setBounce(1, 1);
        this.setMaxVelocity(1000, 1000);
        this.setDamping(false);
    }

    findTarget () {
        const pgroup: Phaser.GameObjects.Group = (this.scene as any).playersGroup;
        let distSq = Number.MAX_VALUE;
        const tmpVec = new Vector2(this.x, this.y);
        pgroup.children.iterate((ship: Ship) => {
            if (ship.isDead()) return;
            const d = tmpVec.set(ship.x - this.x, ship.y - this.y).lengthSq();
            if (d < distSq) {
                distSq = d;
                this.target.setTo(ship.x, ship.y);
                this.target.add(new Vector2(ship.x - this.x, ship.y - this.y).normalize().scale(256));
            }
        });
    }

    update (t, delta) {
        super.update(t, delta)
        if (!this.spawned) return;
        if (this.charging) {
            this.chargeTime -= delta;
            if (this.chargeTime <= 0) {
                this.chargeTime = Circle.CHARGE_TIME + Math.random() * 1000;
                this.runTime = Circle.MAX_RUN_TIME;
                this.charging = false;
                this.findTarget();
                this.scene.physics.moveToObject(this, this.target, 240, 3000);
            }
        } else {
            this.runTime -= delta;
            let closeEnough = Math.abs(this.body.position.distance(this.target)) < 64;
            if (this.runTime <= 0 || closeEnough) {
                this.charging = true;
                this.runTime = Circle.MAX_RUN_TIME;
                this.chargeTime = Circle.CHARGE_TIME + Math.random() * 1000;
                this.body.stop();
            }
        }
    }

}


export class Triangle extends Enemy {
    target: Ship = null;

    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'triangle', 0xFFFF00);
        this.setCircle(36);
        this.setOffset(20, 28)
        this.setMass(20);
        this.setFriction(0, 0);
        this.setGravity(0, 0);
        this.setDrag(0,0);
        this.setGravity(0, 0);
        this.setBounce(1, 1);
        this.setMaxVelocity(1000, 1000);
        this.setDamping(false);
    }

    findTarget (): Ship {
        let target = null;
        const pgroup: Phaser.GameObjects.Group = (this.scene as any).playersGroup;
        let distSq = Number.MAX_VALUE;
        const tmpVec = new Vector2(this.x, this.y);
        pgroup.children.iterate((ship: Ship) => {
            if (ship.isDead() || ship.respawning) return;
            const d = tmpVec.set(ship.x - this.x, ship.y - this.y).lengthSq();
            if (d < distSq) {
                distSq = d;
                target = ship;
            }
        });
        return target;
    }

    update(t, delta) {
        super.update(t, delta);
        if (!this.spawned) return;
        if (this.target === null || this.target.isDead() || this.target.respawning) {
            this.target = this.findTarget();
        }
        if (this.target !== null) {
            this.setRotation(new Vector2(this.target.x - this.x, this.target.y - this.y).angle());
            this.scene.physics.velocityFromRotation(this.rotation, 450, this.body.velocity);
        } else {
            this.setRotation(this.body.velocity.angle())
        }
    }
}


export class Quadrangle extends Enemy {

    target: Ship = null;

    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, 'quadrangle', 0xFFFFFF);
        this.setCircle(64);
        this.setOffset(16, 16)
        this.setMass(20);
        this.setFriction(0, 0);
        this.setGravity(0, 0);
        this.setDrag(0,0);
        this.setAngularDrag(25);
        this.setGravity(0, 0);
        this.setBounce(1, 1);
        this.setMaxVelocity(1000, 1000);
        this.setDamping(false);
    }

    explode() {
        super.explode();
        console.log('spawning triangles');
        let tri = new Triangle(this.scene, this.x - 32, this.y);
        this.wave.enemies.push(tri);
        (this.scene as any).enemiesGroup.add(tri);
        tri = new Triangle(this.scene, this.x, this.y - 32);
        tri.setAngle(90);
        this.wave.enemies.push(tri);
        (this.scene as any).enemiesGroup.add(tri);
        tri = new Triangle(this.scene, this.x + 32, this.y);
        tri.setAngle(180);
        this.wave.enemies.push(tri);
        (this.scene as any).enemiesGroup.add(tri);
        tri = new Triangle(this.scene, this.x, this.y + 32);
        tri.setAngle(-90);
        this.wave.enemies.push(tri);
        (this.scene as any).enemiesGroup.add(tri);
        console.log('spawned triangles');

    }

    findTarget (): Ship {
        let target = null;
        const pgroup: Phaser.GameObjects.Group = (this.scene as any).playersGroup;
        let distSq = Number.MAX_VALUE;
        const tmpVec = new Vector2(this.x, this.y);
        pgroup.children.iterate((ship: Ship) => {
            if (ship.isDead() || ship.respawning) return;
            const d = tmpVec.set(ship.x - this.x, ship.y - this.y).lengthSq();
            if (d < distSq) {
                distSq = d;
                target = ship;
            }
        });
        return target;
    }

    update(t, delta) {
        super.update(t, delta);
        if (!this.spawned) return;
        const cycleT = t % 10000;
        if (cycleT < 5000) {
            this.setAngularDrag(0);
            this.setAngularVelocity(cycleT/2)
            this.phased = cycleT > 1000;
        } else if (this.phased) {
            this.phased = false;
            this.setAngularDrag(300);
            this.target = this.findTarget();
            if(this.target !== null) {
                const v = new Phaser.Math.Vector2(this.target.x - this.x, this.target.y - this.y)
                    .normalize().scale(500);
                this.setVelocity(v.x, v.y);
            }
        }
        this.setAlpha(this.phased ? 0.4 : 1);
    }
}
