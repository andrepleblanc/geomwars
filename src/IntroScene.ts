import * as Phaser from "phaser";
import Player from "./player";

const fontStyle = {
    fontFamily: 'Audiowide',
    fontWeight: 'bold',
    shadow: {
        stroke: true,
        fill: true,
        offsetX: 2,
        offsetY: 2,
        blur: 0
    },
    stroke: '#00AAFF',
    strokeThickness: 15,
    fill: '#FFFFFF',
    align: 'center',
    fontSize: 192
}

class IntroPlayerBox extends Phaser.GameObjects.Container {
    inputIcons = [];
    index: number;
    text: Phaser.GameObjects.Text;
    bg: Phaser.GameObjects.Image;
    static Colors: number[] = [0x00FF00, 0xFF0000, 0xFFFF00, 0x00AAFF];
    constructor(scene, x, y, index) {
        super(scene, x, y);
        this.index = index;
        this.bg = this.scene.add.image(0, 0, 'pibox');
        this.text = this.scene.add.text(0, -76, `Player ${this.index + 1}`, {
            fontFamily: 'Audiowide',
            fontSize: 32,
            color: this.intToHex(IntroPlayerBox.Colors[this.index]),
            stroke: '#000000',
            strokeThickness: 3
        }).setOrigin(0.5);
        this.add(this.bg);
        this.add(this.text);
    }

    addInputIcon (inputIcon) {
        this.inputIcons.push(inputIcon);
        this.add(inputIcon);
        this.positionIcons();
    }

    removeInputIcon (inputIcon) {
        this.inputIcons.splice(this.inputIcons.indexOf(inputIcon), 1);
        this.remove(inputIcon);
        this.positionIcons();
    }

    intToHex(intColor: number) {
        return '#' + intColor.toString(16).padStart(6, '0');
    }

    private positionIcons() {
        for (let i=0; i<this.inputIcons.length; i++) {
            let row = Math.floor(i / 2);
            let col = i % 2;
            this.inputIcons[i].setPosition(-32 + (col * 64), -16 + (row * 64))
        }
    }

    getPlayerColor() {
        return IntroPlayerBox.Colors[this.index];
    }
}

export class GamepadIcon extends Phaser.GameObjects.Container {
    index: number;
    text: Phaser.GameObjects.Text;
    private bg: Phaser.GameObjects.Image;

    constructor(scene, x, y, index) {
        super(scene, x, y);
        this.index = index;

        this.bg = this.scene.add.image(0, 0, 'gamepad');
        this.text = this.scene.add.text(0, -24, `${this.index + 1}`, {
            fontFamily: 'Arial Black',
            fontSize: 22,
            color: '#000000',
        }).setOrigin(0.5);
        this.add(this.bg);
        this.add(this.text);
    }
}


export class IntroScene extends Phaser.Scene {
    gamepadIcons = new Map();
    playerBoxes: IntroPlayerBox[];
    cursorPos = 0;
    playerCount: number = 4;
    cursorLeft: Phaser.GameObjects.Image;
    cursorRight: Phaser.GameObjects.Image;
    bg: Phaser.GameObjects.Image;
    kbmIcon: Phaser.GameObjects.Image;
    touchIcon: Phaser.GameObjects.Image;
    fullscreenButton: Phaser.GameObjects.Image;
    private click: Phaser.Sound.BaseSound;

    constructor(config: string | Phaser.Types.Scenes.SettingsConfig) {
        super('intro');
    }

    preload () {
        this.load.image('bg', 'assets/images/background1.jpg');
        this.load.image('cursor', 'assets/images/cursors.png');
        this.load.image('pibox', 'assets/images/pibox.png');
        this.load.image('gamepad', 'assets/images/gamepad.png');
        this.load.image('kbm', 'assets/images/kbm.png');
        this.load.image('touch', 'assets/images/touch.png');
        this.load.image('fullscreen', 'assets/images/fullscreen.png');
        this.load.image('fullscreen_exit', 'assets/images/fullscreen-exit.png');
        this.load.audio('click', ['assets/audio/click.ogg', 'assets/audio/click.mp3'])
    }


    create() {
        console.log("INtroScene creating")
        this.gamepadIcons.clear();
        this.input.gamepad.on('connected', this.onGamepadConnected, this);
        this.input.gamepad.on('disconnected', this.onGamepadDisconnected, this);
        this.input.gamepad.on('down', this.onGamepadButtonDown, this);
        this.bg = this.add.image(1920/2, 1080/2, 'bg').setOrigin(0.5, 0.5);
        this.fullscreenButton = this.add.image(1920, 0, 'fullscreen');
        this.fullscreenButton.setOrigin(1, 0);
        this.fullscreenButton.setScale(3);
        this.fullscreenButton.setInteractive();
        this.fullscreenButton.on('pointerdown', () => {
            this.toggleFullscreen();
            console.log('FS!');
        })

        const txt = this.add.text(
            1920/2,
            192,
            "SHAPEY\nB A T T L E S",
            fontStyle).setOrigin(0.5, 0.5).setLineSpacing(-80);
        this.moveBG();
        txt.setScale(0, 0);
        this.cursorLeft = this.add.image(512, 1080/2, 'cursor');
        this.cursorRight = this.add.image(1920-512, 1080/2, 'cursor').setFlipX(true);

        const startText = this.add.text(1920/2, 1080 - 128, "START", fontStyle).setFontSize(92).setScale(0, 0).setOrigin(0.5, 0.5);
        startText.setInteractive();


        startText.on('pointerdown', this.go, this);
        this.tweens.add({
            targets: txt,
            yoyo: true,
            repeat:-1,
            duration: 2000
        })

        this.click = this.sound.add('click');
        this.tweens.add({
            targets: [txt, startText],
            scale: 1,
            ease: 'Bounce.easeOut',
            duration: 1000
        })
        this.input.keyboard.on('keydown-DOWN', () => { this.menuDown() })
        this.input.keyboard.on('keydown-UP', () => { this.menuUp() })
        this.input.keyboard.on('keydown-S', () => { this.menuDown() })
        this.input.keyboard.on('keydown-W', () => { this.menuUp() })
        this.input.keyboard.on('keydown-LEFT', () => { this.menuLeft() })
        this.input.keyboard.on('keydown-RIGHT', () => { this.menuRight() })
        this.input.keyboard.on('keydown-A', () => { this.menuLeft() })
        this.input.keyboard.on('keydown-D', () => { this.menuRight() })
        this.input.keyboard.on('keydown-ENTER', () => { this.menuEnter() })
        this.input.keyboard.on('keydown-SPACE', () => { this.menuEnter() })
        this.playerBoxes = [
            new IntroPlayerBox(this, 1920 / 2 - 384, 1080 / 2 + 64, 0),
            new IntroPlayerBox(this, 1920 / 2 - 128, 1080 / 2 + 64, 1),
            new IntroPlayerBox(this, 1920 / 2 + 128, 1080 / 2 + 64, 2),
            new IntroPlayerBox(this, 1920 / 2 + 384, 1080 / 2 + 64, 3)
        ]
        this.playerBoxes.forEach(b => {
           b.bg.setInteractive();
           b.bg.on('pointerdown', (e) => {
               const icon = e.wasTouch ? this.touchIcon : this.kbmIcon;
               (icon.parentContainer as IntroPlayerBox).removeInputIcon(icon);
               b.addInputIcon(icon);
           })
        })
        this.add.existing(this.playerBoxes[0]);
        this.add.existing(this.playerBoxes[1]);
        this.add.existing(this.playerBoxes[2]);
        this.add.existing(this.playerBoxes[3]);
        this.kbmIcon = this.add.image(-512, -512, 'kbm').setScale(0.5, 0.5)
        this.playerBoxes[0].addInputIcon(this.kbmIcon);
        if (isTouchDevice()) {
            this.touchIcon = this.add.image(-512, -512, 'touch').setScale(0.5, 0.5)
            this.playerBoxes[0].addInputIcon(this.touchIcon);
        }
        this.input.gamepad.gamepads.forEach(p => {
            console.log("gamepad on start");
            this.onGamepadConnected(p);
        })
        this.animateMenu();
    }

    moveBG () {
        const bgTween = this.tweens.add({
            targets: this.bg,
            alpha: Math.random() * 0.5 + 0.1,
            x: 512 - Math.random() * 512,
            y: 512 - Math.random() * 512,
            scale: 1 + Math.random() * 1.5,
            ease: 'Quad.easeInOut',
            angle: 90 - Math.random() * 90,
            duration: Math.random() * 3000 + 3000,
            onComplete: () => {
                this.moveBG()
            },
        });

    }


    menuEnter () {
        if (this.cursorPos === 1) {
            this.click.play();
            this.go();
        }
    }

    menuUp (gamepadNumber?) {
        this.click.play();
        this.cursorPos = Math.max(0, this.cursorPos - 1);
        this.animateMenu();
    }

    menuDown (gamepadNumber?) {
        this.click.play();
        this.cursorPos = Math.min(1, this.cursorPos + 1);
        this.animateMenu()
    }

    menuLeft (gamepadNumber?) {
        if (this.cursorPos === 0) {
            let icon;
            if (gamepadNumber === undefined || gamepadNumber === -1) {
                // move KBM icon right
                icon = this.kbmIcon;
            } else {
                icon = this.gamepadIcons.get(gamepadNumber);
            }
            // move KBM icon left
            const curPos = (icon.parentContainer as IntroPlayerBox).index;
            if (curPos > 0) {
                this.click.play();
                (icon.parentContainer as IntroPlayerBox).removeInputIcon(icon);
                this.playerBoxes[curPos-1].addInputIcon(icon);
            }
        }
    }
    menuRight (gamepadNumber?) {
        if (this.cursorPos === 0) {
            let icon;
            if (gamepadNumber === undefined || gamepadNumber === -1) {
                // move KBM icon right
                icon = this.kbmIcon;
            } else {
                icon = this.gamepadIcons.get(gamepadNumber);
            }
            const curPos = (icon.parentContainer as IntroPlayerBox).index;
            if (curPos < this.playerCount-1) {
                this.click.play();
                (icon.parentContainer as IntroPlayerBox).removeInputIcon(icon);
                this.playerBoxes[curPos+1].addInputIcon(icon);
            }
        }
    }

    animateMenu () {
        let leftX = this.cursorPos === 0 ? 384 : 640;
        const rightX = 1920 - leftX;
        let newY = 1080 / 2 + 64;
        if (this.cursorPos === 1) {
            newY = 1080 - 128;
        }
        this.tweens.add({
            targets: [this.cursorLeft, this.cursorRight],
            duration: 100,
            y: newY,
            ease: 'Cubic.easeInOut'
        })
        this.tweens.add({
            targets: [this.cursorLeft],
            duration: 100,
            x: leftX,
            ease: 'Bounce.easeInOut'
        })
        this.tweens.add({
            targets: [this.cursorRight],
            duration: 100,
            x: rightX,
            ease: 'Bounce.easeInOut'
        })
    }

    onGamepadConnected (gamepad) {
        if (this.gamepadIcons.has(gamepad.index)) {
            return;
        }
        const icon = new GamepadIcon(this, -512, -512, gamepad.index);
        this.add.existing(icon);
        this.gamepadIcons.set(gamepad.index, icon);
        this.playerBoxes[0].addInputIcon(icon);
    }

    onGamepadDisconnected (gamepad) {
        const icon = this.gamepadIcons.get(gamepad);
        (icon.parentContainer as IntroPlayerBox).removeInputIcon(icon);
        icon.destroy();
        this.gamepadIcons.delete(gamepad.index);
    }

    onGamepadButtonDown (pad, button, down) {
        if (!this.gamepadIcons.has(pad.index)) {
            this.onGamepadConnected(pad)
        }
        if (button.index === 12) {
            this.menuUp(pad.index);
        } else if (button.index === 13) {
            this.menuDown(pad.index);
        } else if (button.index === 14) {
            this.menuLeft(pad.index);
        } else if (button.index === 15) {
            this.menuRight(pad.index);
        } else if (button.index === 0) {
            this.go();
        }
        console.log(pad.index, button, down);
    }

    update(t, delta) {
        this.input.gamepad.gamepads.forEach((pad: any) => {
            if (!pad.locked) {
                if (pad.leftStick.y > 0.75) {
                    this.menuDown(pad.index);
                    pad.locked = true;
                } else if (pad.leftStick.y < -0.75) {
                    this.menuUp(pad.index);
                    pad.locked = true;
                } else if (pad.leftStick.x > 0.75) {
                    this.menuRight(pad.index);
                    pad.locked = true;
                } else if (pad.leftStick.x < -0.75) {
                    this.menuLeft(pad.index);
                    pad.locked = true;
                }
            } else if (pad.leftStick.length() < 0.2) {
                pad.locked = false;
            }
        });

    }

    go () {
       const data = this.playerBoxes.filter(b => b.inputIcons.length > 0).map((box, i) => {
           const useKBM = box.inputIcons.findIndex(i => i === this.kbmIcon) !== -1;
           const padNum = box.inputIcons.filter(i => i !== this.kbmIcon).map(i => i.index)[0];
           const useTouch = box.inputIcons.findIndex(i => i === this.touchIcon) !== -1;
           return new Player(i, `Player #${box.index+1}`, padNum === undefined ? -1 : padNum, useKBM, useTouch, box.getPlayerColor())
       })
        this.scene.stop();
        this.scene.start('Game', { players: data })
    }

    toggleFullscreen () {
        console.log('toggling fs');
        if (this.scale.isFullscreen) {
            this.scale.stopFullscreen()
            this.fullscreenButton.setTexture('fullscreen');
        } else {
            this.scale.startFullscreen();
            this.fullscreenButton.setTexture('fullscreen_exit');
        }
    }
}

function isTouchDevice() {

    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');

    var mq = function (query) {
        return window.matchMedia(query).matches;
    }

    // @ts-ignore
    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}