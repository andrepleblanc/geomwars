import * as Phaser from 'phaser';
import Text = Phaser.GameObjects.Text;

export class GameOverScene extends Phaser.Scene {
    scrolling: boolean = false;
    credits: Credit[];
    creditsHeight: number = 0;
    title: Text;

    constructor(config) {
        super('gameover');
    }

    addCredit (title, names) {
        const credit: Credit = this.add.existing(new Credit(this, 1920/2, 1200 + this.creditsHeight, title, names)) as Credit;
        this.credits.push(credit);
        this.creditsHeight += credit.getTextHeight() + 96;
    }

    create () {
        setTimeout(() => {
            this.input.keyboard.on('keydown', () => {
                this.scene.stop()
                this.scene.stop('Game')
                this.scene.stop('hud');
                this.scene.launch('intro')

            })
            this.input.gamepad.on('down', (pad, button, value) => {
                this.scene.stop()
                this.scene.stop('Game')
                this.scene.stop('hud');
                this.scene.launch('intro')
            })
        }, 1000);
        this.credits = []
        const bg = this.add.rectangle(0, 0, 1920, 1080, 0x000000, 1).setOrigin(0,0);
        this.title = this.add.text(1920/2, 1080/2, "GAME\nOVER", {
            fontFamily: "Audiowide",
            fontSize: 140,
            fontWeight: 'bold',
            shadow: {
                stroke: true,
                fill: true,
                offsetX: 2,
                offsetY: 2,
                blur: 0
            },
            stroke: '#00AAFF',
            strokeThickness: 15,
            fill: '#FFFFFF',
            align: 'center',
        }).setOrigin(0.5, 0.5);
        this.addCredit("Programmer", "André LeBlanc")
        this.addCredit("Graphic Artist", "André LeBlanc")
        this.addCredit("Sound Effects Artist", "Mike Ulch")
        this.addCredit("Level Designer", "Mike Ulch")
        this.addCredit("Concept By", "André LeBlanc &\n   Mike Ulch")
        this.addCredit("\n\n\n\n\n\n", "Thanks For Playing!")

        this.tweens.add({
            targets: bg,
            alpha: { from: 0.33, to: 0.9 },
            duration: 1000,
            onComplete: () => {
                this.startScrolling();
            }
        })

    }

    private startScrolling() {
        this.scrolling = true;
    }

    update (t, delta) {
        if (this.scrolling) {
            this.title.y -= delta / 5;
            this.credits.forEach(c => {
                c.y -= delta / 5;
            })
            if (this.title.y < -this.creditsHeight) {
                this.scrolling = false;
            }
        }

    }
}

class Credit extends Phaser.GameObjects.Container {
    private titleText: Text;
    private namesText: Text;

    constructor(scene: Phaser.Scene, x: number, y: number, title: string, names: string) {
        super(scene, x, y);
        this.titleText = this.scene.add.text(0, 0, title, {
            fontFamily: "Audiowide",
            fontSize: 38,
            fontWeight: 'bold',
            shadow: {
                stroke: true,
                fill: true,
                offsetX: 2,
                offsetY: 2,
                blur: 0
            },
            stroke: '#00AAFF',
            strokeThickness: 15,
            fill: '#FFFFFF',
            align: 'center',
        }).setOrigin(0.5, 0);

        this.namesText = this.scene.add.text(0, 70, names, {
            fontFamily: "Audiowide",
            fontSize: 54,
            fontWeight: 'bold',
            shadow: {
                stroke: true,
                fill: true,
                offsetX: 2,
                offsetY: 2,
                blur: 0
            },
            stroke: '#00AAFF',
            strokeThickness: 15,
            fill: '#FFFFFF',
            align: 'center',
        }).setOrigin(0.5, 0);
        this.add(this.titleText);
        this.add(this.namesText);
    }

    getTextHeight(): number {
        return this.titleText.getBounds().height + this.namesText.getBounds().height;
    }
}