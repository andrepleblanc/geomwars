import * as Phaser from "phaser";
import Player from "./player";
import Vector2 = Phaser.Math.Vector2;
import {GameScene} from "./GameScene";
import {VirtualJoystick} from "./VirtualJoystick";


export class IconCounter extends Phaser.GameObjects.Container {
    texture: string;
    iconWidth = 32
    tint: number;
    rtl: boolean = false;
    constructor(scene: Phaser.Scene, x: number, y: number, texture, initialCount, RTL: boolean = false) {
        super(scene, x, y, []);
        this.texture = texture;
        this.rtl = RTL;
        this.increment(initialCount);
    }

    set (value: number) {
        const change = value - this.length;
        if (change > 0) {
            this.increment(change);
        } else if (change < 0) {
            this.decrement(Math.abs(change));
        }
    }

    increment (amount: number = 1) {
        const existingCount = this.length;
        for (let i=0; i<amount; i++) {
            let x = (existingCount + i) * this.iconWidth;
            if (this.rtl) x *= -1;
            const icon = this.scene.add.image(x, 0, this.texture);
            icon.setTint(this.tint);
            this.add(icon);
        }
    }

    decrement (amount: number = 1) {
        for (let i=0; i<amount && this.length > 0; i++) {
            this.removeAt(this.length-1, true);
        }
    }

    setTint(color: number) {
        this.tint = color;
        this.iterate((child) => {
            child.setTint(this.tint);
        }, this);
    }
}

export class HUDScene extends Phaser.Scene {
    players: Player[];
    scoreObjects: Phaser.GameObjects.Text[];
    bombObject: IconCounter;
    lifeObject: IconCounter;
    bombs: number;
    lives: number;
    gameScene: GameScene;
    leftStick: VirtualJoystick;
    rightStick: VirtualJoystick;
    bombButton: boolean;

    constructor(config: string | Phaser.Types.Scenes.SettingsConfig) {
        super('hud');
    }

    create () {}

    init (data) {
        this.gameScene = data.scene;
        this.players = data.scene.players;
        this.scoreObjects = [];
        this.bombObject = new IconCounter(this, 1880,36, 'bomb', this.gameScene.bombs, true);
        this.lifeObject = new IconCounter(this, 1880, 72, 'ship_icon', this.gameScene.lives, true);
        this.add.existing(this.lifeObject);
        this.add.existing(this.bombObject);


        for (let i=0; i<this.players.length; i++) {
            const p = this.players[i];
            if (p.useTouch) {
                this.addTouchControls();
            }
            this.scoreObjects[i] = this.add.text(16, 12 + i*32, `${p.name}: ${p.score}`, { fontSize: 32, fontFamily: 'Audiowide' });
            this.scoreObjects[i].setShadow(0, 0, "#000000", 5, true, true);
            this.scoreObjects[i].setTint(p.color);


        }
    }

    addTouchControls() {
        const bombButton = this.add.graphics({ x: 900, y: 0 });
        bombButton.setInteractive(new Phaser.Geom.Rectangle(0, 0, 120, 1080), (hitArea, x, y) => {
            return hitArea.contains(x, y);
        });
        bombButton.on('pointerdown', () => {
            console.log('bb');
            this.bombButton = true;
        })
        bombButton.on('pointerup', () => {
            this.bombButton = false;
        })
        bombButton.on('pointerout', () => {
            this.bombButton = false;
        })

        this.leftStick = new VirtualJoystick(this, 0, 0, 900, 1080);
        this.rightStick = new VirtualJoystick(this, 1020, 0, 900, 1080);
        this.add.existing(this.leftStick);
        this.add.existing(this.rightStick);
    }

    update () {
        this.bombObject.set(this.gameScene.bombs);
        this.lifeObject.set(this.gameScene.lives);
        for (let i=0; i<this.players.length; i++) {
            const p = this.players[i];
            this.scoreObjects[i].setText(`${p.name}: ${p.score}`);
            if (p.ship.isOffscreen()) {
                const c = p.ship.getScene().cameras.main.worldView;
                if (p.ship.cursor === null) {
                    p.ship.cursor = this.add.image(0, 0, 'ship_cursor')
                    p.ship.cursor.setTint(p.color);
                }
                const pt = pointOnRect(p.ship.x, p.ship.y, c.left + 64, c.top + 64, c.right - 64, c.bottom - 64, false)
                pt.x -= c.left;
                pt.y -= c.top;
                const angle = new Vector2(p.ship.x - c.centerX, p.ship.y - c.centerY).angle();
                p.ship.cursor.setRotation(angle);
                p.ship.cursor.setPosition(pt.x, pt.y);
            } else if (p.ship.cursor !== null) {
                p.ship.cursor.destroy();
                p.ship.cursor = null;
            }
        }
    }
}



function pointOnRect(x, y, minX, minY, maxX, maxY, validate) {
    //assert minX <= maxX;
    //assert minY <= maxY;
    if (validate && (minX < x && x < maxX) && (minY < y && y < maxY))
        throw "Point " + [x,y] + "cannot be inside "
        + "the rectangle: " + [minX, minY] + " - " + [maxX, maxY] + ".";
    var midX = (minX + maxX) / 2;
    var midY = (minY + maxY) / 2;
    // if (midX - x == 0) -> m == ±Inf -> minYx/maxYx == x (because value / ±Inf = ±0)
    var m = (midY - y) / (midX - x);

    if (x <= midX) { // check "left" side
        var minXy = m * (minX - x) + y;
        if (minY <= minXy && minXy <= maxY)
            return {x: minX, y: minXy};
    }

    if (x >= midX) { // check "right" side
        var maxXy = m * (maxX - x) + y;
        if (minY <= maxXy && maxXy <= maxY)
            return {x: maxX, y: maxXy};
    }

    if (y <= midY) { // check "top" side
        var minYx = (minY - y) / m + x;
        if (minX <= minYx && minYx <= maxX)
            return {x: minYx, y: minY};
    }

    if (y >= midY) { // check "bottom" side
        var maxYx = (maxY - y) / m + x;
        if (minX <= maxYx && maxYx <= maxX)
            return {x: maxYx, y: maxY};
    }

    // edge case when finding midpoint intersection: m = 0/0 = NaN
    if (x === midX && y === midY) return {x: x, y: y};

    // Should never happen :) If it does, please tell me!
    throw "Cannot find intersection for " + [x,y]
    + " inside rectangle " + [minX, minY] + " - " + [maxX, maxY] + ".";
}