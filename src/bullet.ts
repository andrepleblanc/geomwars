import * as Phaser from "phaser";
import {WORLD_HEIGHT, WORLD_WIDTH} from "./constants";
import Vector2 = Phaser.Math.Vector2;

export default class Bullet extends Phaser.Physics.Arcade.Image {
    static COOLDOWN: number = 350;

    constructor(scene, x, y) {
        super(scene, x, y, 'bullet');
        scene.physics.add.existing(this)
        this.setCollideWorldBounds(false);
        scene.add.existing(this)
        this.disableBody(true, true);
    }

    fire (x: number, y: number, playerIndex: 0, direction: Vector2, tint: number) {
        this.setData('shooter', playerIndex);
        this.setRotation(direction.angle());
        const offset = direction.scale(40);
        this.setX(x + offset.x);
        this.setY(y + offset.y);
        this.setTint(tint);
        this.enableBody(false, x, y, true, true);
        const velocityVector = direction.normalize().scale(2000)
        this.setVelocity(velocityVector.x, velocityVector.y);
    }

    update () {
        if (this.x < 0 || this.x > WORLD_WIDTH || this.y < 0 || this.y > WORLD_HEIGHT) {
            this.disableBody(true, true);
            this.setVelocity(0, 0);
        }
    }

    recycle() {
        this.disableBody(true, true);
        this.setVelocity(0, 0);
    }
}
