import * as Phaser from 'phaser'
import Player from "./player";
import Vector2 = Phaser.Math.Vector2;
import ParticleEmitter = Phaser.GameObjects.Particles.ParticleEmitter;

export default class Ship extends Phaser.Physics.Arcade.Image {
    player: Player;
    exploder: Phaser.GameObjects.Particles.ParticleEmitterManager;
    emitter: ParticleEmitter;
    velocityScale: number = 2000;
    _tint: number = 0xFFFFFF;
    spawnX: number;
    spawnY: number;
    respawning: boolean = false;
    respawnTime: number = 0;
    shield: Phaser.GameObjects.Graphics;
    cursor: Phaser.GameObjects.Image = null;
    gunSound: any;
    bombSound: any;
    thrustStartSound: Phaser.Sound.BaseSound;
    thrustStopSound: Phaser.Sound.BaseSound;
    thrustLoopSound: Phaser.Sound.BaseSound;
    deathSound: Phaser.Sound.BaseSound;

    isOffscreen() {
        return !this.scene.cameras.main.worldView.contains(this.x, this.y);
    }

    setColor(tint: number) {
        this._tint = tint;
        this.exploder.emitters.first.setTint(this._tint);
        this.setTint(tint);
    }

    getColor() {
        return this._tint;
    }

    isDead(): boolean {
        return !this.active;
    }

    respawn() {
        this.respawning = true;
        this.respawnTime = 0;
        this.enableBody(true, this.x, this.y, true, true);
        this.scene.tweens.addCounter({
            from: 32,
            to: 192,
            ease: 'Quad.easeOut',
            duration: 1000,
            onUpdate: (tween) => {
                const v = tween.getValue();
                this.setCircle(v, 64 - v, 64 - v)
            },
        })
    }

    constructor(scene, x, y, key, player) {
        super(scene, x, y, key);
        this.player = player
        this.spawnX = x;
        this.spawnY = y;
        this.shield = scene.add.graphics({
            x: 0,
            y: 0,
            lineStyle: {
                width: 1,
                color: 0xffffff,
                alpha: 1
            }
        });
        scene.physics.add.existing(this, false);
        this.gunSound = this.scene.sound.add('gun');
        this.bombSound = this.scene.sound.add('bomb');
        this.thrustStartSound = this.scene.sound.add('thrust_start')
        this.thrustStopSound = this.scene.sound.add('thrust_stop')
        this.thrustLoopSound = this.scene.sound.add('thrust_loop')
        this.deathSound = this.scene.sound.add('player_death');

        this.setCircle(32, 32, 32);
        this.setDamping(true);
        this.setDrag(0.5, 0.5)
        this.setCollideWorldBounds(true);
        scene.add.existing(this);
        const particles = scene.add.particles('ship');
        this.exploder = scene.add.particles('ship_pieces');
        this.exploder.createEmitter({
            angle: { min: 0, max: 360 },
            lifespan: 666,
            speed: { min: 100, max: 400 },
            quantity: { min: 4, max: 8 },
            scale: { start: 2, end: 0 },
            rotate: { start: 0, end: 360, random: true },
            on: false,
            tint: this._tint
        })
        this.emitter = particles.createEmitter({
            tint: {
                onEmit: (particle, key, value) => {
                    return this._tint;
                }
            },
            speed: 0,
            lifespan: 1000,
            rotate: {
                onEmit: (particle, key, value) => {
                    return this.angle;
                }
            },
            alpha: {
                start: 0.25, end: 0
            },
            quantity: {
                onEmit: (particle, key, value) => {
                    const val = Phaser.Math.Percent((this.body as any).speed, 0, 1000);
                    return val;
                }
            },
            scale: { start: 1, end: 0 },
            blendMode: 'ADD'
        })
        this.emitter.startFollow(this);
    }

    update (t, delta) {
        this.shield.clear();
        this.shield.setPosition(this.x, this.y);
        if (this.respawning) {
            this.respawnTime += delta;
            if (this.respawnTime > 3000) {
                this.respawning = false;
                this.setCircle(32, 32, 32);
            } else {
                this.shield.strokeCircle(0, 0, this.body.radius);
            }
        }
    }

    doMovement(movement: Vector2) {
        const mv = movement.normalize().scale(this.velocityScale);
        this.setVelocity(mv.x, mv.y)
        if (movement.length() > 0.15) {
            this.setRotation(movement.angle())
        }
    }

    getScene() {
        return this.scene;
    }
}
