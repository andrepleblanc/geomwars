import * as Phaser from "phaser";
import {Circle, Enemy, Quadrangle, Triangle} from "./enemies";
import {GameScene} from "./GameScene";
import {WORLD_HEIGHT, WORLD_WIDTH} from "./constants";


export interface SpawnConfig {
    cls: typeof Enemy;
    px?: number;
    py?: number;
    wx?: number;
    wy?: number;
}

export interface WaveConfig {
    duration?: number;
    spawns: SpawnConfig[];
}


export class Wave extends Phaser.Events.EventEmitter {
    waveManager: WaveManager;
    duration: number = -1;
    timeRemaining: number;
    enemies: Enemy[];

    constructor(waveManager: WaveManager) {
        super();
        this.waveManager = waveManager;
    }

    start () {
        this.enemies = [];
        if (this.duration !== -1) {
            this.timeRemaining = this.duration;
        }
    }

    spawn (enemy: Enemy) {
        this.waveManager.scene.add.existing(enemy);
        this.waveManager.scene.enemiesGroup.add(enemy);
        this.enemies.push(enemy);
        this.waveManager.scene.mobSpawnSound.play();
        enemy.wave = this;
    }

    update (t, delta) {
        this.enemies = this.enemies.filter(e => e.active);
        if (this.duration !== -1) {
            this.timeRemaining -= delta;
            if (this.timeRemaining <= 0) {
                this.emit('ended');
            }
        } else if (this.enemies.length === 0) {
            this.emit('ended');
        }
    }
}


export abstract class WaveManager {
    scene: GameScene;

    constructor(scene: GameScene) {
        this.scene = scene;
    }

    abstract getNext (): Wave
}


export class StaticWave extends Wave {

    spawns: SpawnConfig[];

    constructor(waveManager: WaveManager, duration, spawns: SpawnConfig[]) {
        super(waveManager);
        this.duration = duration === undefined ? -1 : duration;
        this.spawns = spawns;
    }

    start() {
        super.start();
        for (let i=0; i < this.spawns.length; i++) {
            const spawnCfg = this.spawns[i];
            let x, y;
            if (spawnCfg.px !== undefined && spawnCfg.py !== undefined) {
                const player = this.waveManager.scene.playersGroup.getFirstAlive(false);
                if (player) {
                    x = Phaser.Math.Clamp(player.x + spawnCfg.px, 0, WORLD_WIDTH);
                    y = Phaser.Math.Clamp(player.y + spawnCfg.py, 0, WORLD_HEIGHT);
                }
            } else {
                x = spawnCfg.wx;
                y = spawnCfg.wy;
            }
            const e = new spawnCfg.cls(this.waveManager.scene, x, y);
            this.spawn(e);
        }
    }

    update(t, delta) {
        super.update(t, delta);
    }
}

export class StaticWaveManager extends WaveManager {
    private configs: WaveConfig[];
    private index: number = 0;

    constructor(scene: GameScene, configs: WaveConfig[]) {
        super(scene);
        this.configs = configs;
        this.index = 0;
    }

    getNext (): Wave {
        if (this.index < this.configs.length) {
            const cfg = this.configs[this.index++];
            return new StaticWave(this, cfg.duration, cfg.spawns);
        } else {
            console.log(this.index, ' > ', this.configs.length, 'over');
        }
    }
}


export class DynamicWaveManager extends WaveManager {
    count: number = 0;

    constructor(scene: GameScene) {
        super(scene);
    }

    getNext(): Wave {
        this.count++;
        let cls = [Circle, Triangle, Quadrangle][Math.floor(Math.random() * 3)]
        let qty = this.count / 2;
        if (qty < 1) {
            qty = 1
        }
        let duration = 500
        if (this.count <= 10) {
            cls = Circle
            duration = 4000
            if (qty > 6) {
                qty = 6
            }
        } else if (this.count > 10 && this.count <= 100) {
            cls = [Circle, Triangle][Math.floor(Math.random() * 2)]
            duration = 4000 - (this.count * 20)
            if (qty > 25) {
                qty = 25
            }
        } else if (this.count > 100) {
            qty = this.count - 70
            duration = 4000
            if (cls == Quadrangle) {
                qty = qty / 2
            }
        }
        if (this.count % 10 == 0) {
            duration = -1
            for (let i=0; i<this.scene.players.length; i++){
                if (this.scene.players[i].fireRate > 100) {
                    this.scene.players[i].fireRate = this.scene.players[i].fireRate - 50
                } else {
                    this.scene.players[i].fireRate = 100
                }
            }
        }
        qty = Math.floor(qty);
        return new StaticWave(this, duration, surroundSpawner(cls, qty, 384 + Math.random() * 768));
        return ;
    }
}


export function surroundSpawner(cls, qty, radius): SpawnConfig[] {
    const configs = [];
    const sliceAngle = Math.PI * 2 / qty;
    for (let i=0; i<qty; i++) {
        const a = sliceAngle * i;
        const x = radius * Math.cos(a);
        const y = radius * Math.sin(a);
        configs.push({cls: cls, px: x, py: y})
    }
    return configs;
}

