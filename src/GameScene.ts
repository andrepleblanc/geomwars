import * as Phaser from "phaser";
import {DynamicWaveManager, StaticWaveManager, Wave, WaveManager} from "./Wave";
import Player from "./player";
import {WAVES} from "./waves";
import Ship from "./ship";
import Bullet from "./bullet";
import {ADD_BOMB_SCORE, INITIAL_BOMBS, INITIAL_LIVES, RESPAWN_DELAY, WORLD_HEIGHT, WORLD_WIDTH} from "./constants";
import {Enemy} from "./enemies";
import {HUDScene} from "./hud";


export class GameScene extends Phaser.Scene {

    bombs: number = INITIAL_BOMBS;
    lives: number = INITIAL_LIVES;

    private bulletsGroup: Phaser.GameObjects.Group;
    playersGroup: Phaser.GameObjects.Group;
    enemiesGroup: Phaser.GameObjects.Group;

    private runningWaves: Wave[] = [];
    players: Player[];
    private isGameOver: boolean = false;
    // private waveManager: WaveManager = new StaticWaveManager(this, WAVES);
    private waveManager: WaveManager;
    mobKillSound: Phaser.Sound.BaseSound;
    playerDeathSound: Phaser.Sound.BaseSound;
    mobSpawnSound: Phaser.Sound.BaseSound;

    constructor() {
        super({
            active: false,
            visible: false,
            key: 'Game',
        });
    }

    init (data: any) {
        this.players = data.players || [];
        this.lives = INITIAL_LIVES;
        this.bombs = INITIAL_BOMBS;
        this.waveManager = new DynamicWaveManager(this);

    }


    public preload() {
        this.load.image('bg', 'assets/images/background1.jpg');
        this.load.image('ship', 'assets/images/ship.png');
        this.load.image('bullet', 'assets/images/bullet.png');
        this.load.image('circle', 'assets/images/circle.png');
        this.load.image('triangle', 'assets/images/triangle.png')
        this.load.image('quadrangle', 'assets/images/quadrangle.png')
        this.load.image('ship_pieces', 'assets/images/ship_pieces.png');
        this.load.image('ship_cursor', 'assets/images/ship_cursor.png');
        this.load.image('ship_icon', 'assets/images/ship_icon.png');
        this.load.image('bomb', 'assets/images/bomb.png');
        this.load.glsl('shaders', 'assets/shaders/shaders.glsl.js');
        this.load.audio('mob_kill', ['assets/audio/mob_kill.ogg', 'assets/audio/mob_kill.mp3'])
        this.load.audio('player_death', ['assets/audio/player_death.ogg', 'assets/audio/player_death.mp3'])
        this.load.audio('mob_spawn', ['assets/audio/mob_spawn.ogg', 'assets/audio/mob_spawn.mp3'])
        this.load.audio('gun', ['assets/audio/player_shot.ogg', 'assets/audio/player_shot.mp3'])
        this.load.audio('bomb', ['assets/audio/bomb.ogg', 'assets/audio/bomb.mp3'])
        this.load.audio('thrust_start', ['assets/audio/thrust_start.ogg', 'assets/audio/thrust_start.mp3'])
        this.load.audio('thrust_loop', ['assets/audio/thrust_loop.ogg', 'assets/audio/thrust_loop.mp3'])
        this.load.audio('thrust_stop', ['assets/audio/thrust_stop.ogg', 'assets/audio/thrust_stop.mp3'])
    }

    public create() {
        this.playersGroup = this.add.group({
            runChildUpdate: true,
            maxSize: 4,
            classType: Ship
        });

        this.bulletsGroup = this.add.group({
            runChildUpdate: true,
            maxSize: 200,
            classType: Bullet
        })
        this.enemiesGroup = this.add.group({
            runChildUpdate: true
        })

        this.bulletsGroup.createMultiple({
            classType: Bullet,
            active: false,
            visible: false,
            frameQuantity: 100,
            repeat: 100,
            key: 'bullet',
            setXY: {
                x: 0,
                y: 0
            }
        })
        this.mobKillSound = this.sound.add('mob_kill');
        this.mobSpawnSound = this.sound.add('mob_spawn');
        this.playerDeathSound = this.sound.add('player_death');
        this.bulletsGroup.setDepth(1)
        this.cameras.main.setBounds(0, 0, WORLD_WIDTH, WORLD_HEIGHT);
        this.physics.world.setBounds(0, 0, WORLD_WIDTH, WORLD_HEIGHT, true, true, true, true);
        this.add.image(0, 0, 'bg').setOrigin(0);
        this.cameras.main.followOffset.set(0, 0);
        this.input.keyboard.addKey('F').on('down', this.toggleFullscreen, this);
        this.input.keyboard.addKey('ESC').on('down', this.pauseGame, this)
        this.input.gamepad.on('down', (pad, button, value) => {
            if (button.index === 16) {
                this.pauseGame();
            }
        })
        this.players.forEach((p, i) => {
            const input = p.createInput(this);
            p.createShip(this, 128 + (128 * i), 512);
            this.playersGroup.add(p.ship);
            p.ship.respawn();
            input.on('bomb', () => {
                if (p.ship.isDead() || this.bombs <= 0) return;
                this.bombs -= 1;
                p.ship.bombSound.play();
                this.enemiesGroup.children.each((e: Enemy) => {
                    this.playerKillEnemy(p, e);
                })
            })

            input.on('startmoving', () => {
                console.log("startmoving");
                p.ship.thrustStartSound.play({ volume: 0.5});
                p.ship.thrustStartSound.once('complete', () => {
                    p.ship.thrustLoopSound.play({ loop: true, volume: 0.5 });
                })
            })
            input.on('stopmoving', () => {
                console.log("stopmoving")
                p.ship.thrustStartSound.stop();
                p.ship.thrustLoopSound.stop();
                p.ship.thrustStopSound.play({ volume: 0.5 });
            })
        });

        this.cameras.main.startFollow(this.players[0].ship);
        this.scene.launch('hud', {scene: this})
        this.nextWave();
        this.physics.add.collider(this.enemiesGroup, this.playersGroup, (enemy: Enemy, ship: Ship) => {
            if (!ship.isDead() && !ship.respawning && enemy.spawned) {
                ship.player.die();
                if (this.lives > 0) {
                    this.lives -= 1;
                    setTimeout(() => {
                        ship.respawn();
                    }, RESPAWN_DELAY);
                } else {
                    this.gameOver();
                }
            }
        }, (enemy, player) => (enemy as Enemy).spawned);

        this.physics.add.overlap(this.enemiesGroup, this.bulletsGroup, (enemy: Enemy, bullet: Bullet) => {
            if (!enemy.spawned) return;
            if (enemy.deflecting) {
                bullet.recycle();
                return;
            } else if (enemy.phased) {
                return;
            }
            const playerIndex = bullet.getData('shooter')
            bullet.recycle();
            this.playerKillEnemy(this.players[playerIndex], enemy);
        })
        this.physics.add.collider(this.enemiesGroup, this.enemiesGroup);

    }

    pauseGame () {
        this.scene.pause();
        setTimeout(() => {
            this.scene.launch('pause')
        }, 100);
    }

    public nextWave() {
        const wave = this.waveManager.getNext();
        if (!wave) {
            console.log("GAME OVER");
            return;
        }
        wave.start()
        wave.once('ended', () => {
            console.log("wave ended");
            this.runningWaves.splice(this.runningWaves.indexOf(wave),1);
            this.nextWave();
        })
        this.runningWaves.push(wave);
    }

    public update(t, delta) {

        for (let i=0; i<this.runningWaves.length; i++) {
            this.runningWaves[i].update(t, delta);
        }

        for (let i=0; i<this.players.length; i++) {
            const player = this.players[i];
            player.update(t, delta)
        }
    }

    toggleFullscreen () {
        console.log('toggling fs');
        if (this.scale.isFullscreen) {
            this.scale.stopFullscreen()
        } else {
            this.scale.startFullscreen();
        }
    }

    private playerKillEnemy(player: Player, enemy: Enemy) {
        this.mobKillSound.play();
        enemy.explode();
        enemy.destroy(false);
        const totalScore = this.players.reduce((val, p) => {
            return val + p.score;
        }, 0)

        if (Math.floor(totalScore / ADD_BOMB_SCORE) < (Math.floor((totalScore + enemy.score) / ADD_BOMB_SCORE))) {
            if (this.bombs < 5) {
                this.bombs += 1;
            }
        }
        player.score += enemy.score;
    }

    private gameOver() {
        this.isGameOver = true;
        this.scene.pause()
        setTimeout(() => {
            this.scene.launch('gameover');
        }, 500);

    }
}