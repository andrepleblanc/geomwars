import {PlayerInput} from "./PlayerInput";
import Ship from "./ship";
import {GameScene} from "./GameScene";

export default class Player {
    index: number;
    gamepadIndex: number = -1;
    useKBM: boolean = true;
    name: string;
   private  _score: number = 0;
    color: number = 0xFFFFFF;
    input: PlayerInput;
    ship: Ship;
    private _fireCooldown: number = 0;
    fireRate: number = 350;
    useTouch: boolean;

    constructor(index: number, name: string, gamepadIndex: number, useKBM: boolean, useTouch: boolean, color: number) {
        this.index = index;
        this.gamepadIndex = gamepadIndex;
        this.useKBM = useKBM;
        this.useTouch = useTouch;
        this.name = name;
        this.color = color;
    }

    get score () {
        return this._score;
    }

    die () {
        this.ship.deathSound.play();
        this.ship.thrustStopSound.stop();
        this.ship.thrustLoopSound.stop();
        this.ship.thrustStartSound.stop();
        this.ship.disableBody(true, true);
        this.ship.emit('death');
        this.ship.exploder.emitParticleAt(this.ship.x, this.ship.y);
    }

    set score (val) {
        this._score = val;
    }

    createInput(scene: GameScene): PlayerInput {
        this.input = new PlayerInput(scene, this.useKBM, this.useTouch, this.gamepadIndex);
        return this.input;
    }

    createShip(scene: Phaser.Scene, x: number, y: number): Ship {
        this.ship = new Ship(scene, x, y, 'ship', this);
        this.ship.setColor(this.color);
        return this.ship;
    }

    doFiring(firingVector: Phaser.Math.Vector2, bulletsGroup) {
        if (this._fireCooldown <= 0 && this.input.firingVector.length() > 0.15) {
            const bullet = bulletsGroup.getFirstDead(false);
            this.ship.gunSound.play();
            bullet.fire(this.ship.x, this.ship.y, this.index, firingVector, this.color);
            this._fireCooldown = this.fireRate;
        }
    }

    update(t: number, delta: number) {
        this._fireCooldown -= delta;
        if (!this.ship.isDead()) {
            this.input.update(this.ship, this.ship.getScene().cameras.main);
            this.ship.doMovement(this.input.movementVector);
            const bulletGroup = (this.ship.getScene() as any).bulletsGroup;
            this.doFiring(this.input.firingVector, bulletGroup);
        }
    }
}
