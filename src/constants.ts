export const WORLD_HEIGHT = 1080 * 2;
export const WORLD_WIDTH = 1920 * 2;
export const ADD_BOMB_SCORE = 15000;
export const INITIAL_LIVES = 2;
export const INITIAL_BOMBS = 4;
export const RESPAWN_DELAY = 2000;