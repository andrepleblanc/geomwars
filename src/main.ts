import * as Phaser from 'phaser';
import {IntroScene} from "./IntroScene";
import {HUDScene} from "./hud";
import {GameScene} from "./GameScene";
import {PauseScene} from "./PauseScene";
import {GameOverScene} from "./GameOverScene";

const gameConfig = {
  title: 'Sample',

  type: Phaser.AUTO,
  width: 1920,
  height: 1080,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH
  },
  input: {
    gamepad: true
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        x:  0,
        y:  0
      },
      debug: false
    },
  },
  fps: {
    smoothStep: false,
  },
  scene: [IntroScene, GameScene, HUDScene, PauseScene, GameOverScene],
  parent: 'game',
  backgroundColor: '#000000',
};

export const game = new Phaser.Game(
    gameConfig
);
