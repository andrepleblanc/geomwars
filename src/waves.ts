import {Circle, Quadrangle, Triangle} from "./enemies";
import Ship from "./ship";
import {surroundSpawner} from "./Wave";

const WAVES = [
    { spawns: [{cls: Quadrangle, wx: 1920, wy: 1080}] },
    { spawns: surroundSpawner(Circle, 8, 192) },
    { spawns: [
        { cls: Circle, wx: 1920, wy: 1000 },
        { cls: Circle, wx: 1920, wy: 1000 },
        { cls: Circle, wx: 1920, wy: 1000 },
        { cls: Circle, wx: 1920, wy: 1080 },
        { cls: Circle, wx: 1920, wy: 1080 },
        { cls: Circle, wx: 1920, wy: 1080 },
        { cls: Circle, wx: 1920, wy: 1160 },
        { cls: Circle, wx: 1920, wy: 1160 },
        { cls: Circle, wx: 1920, wy: 1160 }
    ]},
    { spawns: [
        {cls: Quadrangle, wx: 128, wy: 128},
        {cls: Quadrangle, wx: 3712, wy: 128},
        {cls: Quadrangle, wx: 128, wy: 2032},
        {cls: Quadrangle, wx: 3712, wy: 2032},
    ]}
]

export { WAVES }