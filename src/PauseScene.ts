import * as Phaser from 'phaser';

export class PauseScene extends Phaser.Scene {

    constructor(config) {
        super('pause');
    }

    create () {
        this.input.keyboard.on('keydown', () => {
            this.scene.stop();
            this.scene.resume('Game')
        })
        this.input.gamepad.on('down', (pad, button, value) => {
            console.log("UNPAUSE");
            this.scene.stop();
            this.scene.resume('Game');
        })
        const bg = this.add.graphics({ x: 0, y: 0, fillStyle: { color: 0, alpha: 0.33 }})
        bg.fillRect(0, 0, 1920, 1080);
        const txt1 = this.add.text(1920/2, 1080/2, "PAUSED", {
            fontFamily: "Audiowide",
            fontSize: 90,
            fontWeight: 'bold',
            shadow: {
                stroke: true,
                fill: true,
                offsetX: 2,
                offsetY: 2,
                blur: 0
            },
            stroke: '#00AAFF',
            strokeThickness: 15,
            fill: '#FFFFFF',
            align: 'center',
        }).setOrigin(0.5, 0.5)

    }
}